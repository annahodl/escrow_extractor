# Escrow extraction tool

Single-page application to create transactions for extracting funds from escrow.

## Developing and building

```
# Start web-server
yarn install
yarn start
```

```
# Build
yarn build
```

## Usage

Open `dist/index.html` in a browser.

Prapare to enter:

* *encrypted seed*
* *payment password*
* *derivation index*
* *escrow address*
* *witness script*

Collaborate with your counterpary (owner of the second key from escrow) and split the roles. One of you would be **transaction creator** and another one a **broadcaster**. Then follow the corresponding workflow.

## Transaction creator

Select desired blockchain network at the top.

1. Fill in ①: *encrypted seed*, *payment password*, *derivation index*. Ensure *private key* appeared.
2. Fill in ②:
    - *escrow address*, and wait for *UTXOs (JSON)* to appear (they will be fetched automatically from a blockchain explorer)
    - *witness script*, ensure it is validated as correct (green hint at the bottom of the witness script input appears)
3. Fill in ③:
    - Enter desired *output address 1*, *value 1* (in satoshis), *output address 2*, *value 2* (in satoshis). Agree with the other party on outputs and values beforehand. Typically, first output is your own address, and second output is the other party's address.
    - Check that values *Goes to outputs*, *Network fee*, *Recommended network fee* appeared, are valid and correct. **Note that funds from escrow which don't go to outputs will go to miners as their fee**. Increase or decrease outputs' values accordingly so that *Network fee* is equal to *Recommended fee*.
    - Ensure *skeleton TX hex* appeared
4. Copy from ④:
    - Copy *Partially signed TX* and send it to the broadcaster.

## Broadcaster

Select desired blockchain network at the top.

1. Get partially signed TX (hex string) from the transaction creator
2. Fill in ①: *encrypted seed*, *payment password*, *derivation index*. Ensure *private key* appeared.
3. Fill in ③: only paste received hex string into *Skeleton TX hex*. Leave everything else in this section blank.
4. Validate ④:
    - Ensure *TX* appeared.
    - **Independently validate TX** (do not skip step)
      - For example, open this transaction decoder: https://live.blockcypher.com/btc/decodetx/
      - Select blockchain network at the bottom ("Bitcoin" for mainnet, "Bitcoin testnet" for testnet)
      - Paste transaction hex to the corresponding field
      - Click "Decode transaction"
      - Check that input is escrow address and output addresses and values are the same that you agreed beforehand with the counterparty
    - Click "Broadcast", ensure transaction was broadcasted (green hint appeared below button and transaction is displayed in a blockchain explorer or wallet)
