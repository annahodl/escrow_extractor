import * as bitcoin from 'bitcoinjs-lib'

function throttle (callback, limit) {
  var waiting = false;
  return function () {
    if (!waiting) {
      callback.apply(this, arguments);
      waiting = true;
      setTimeout(function () {
        waiting = false;
      }, limit);
    }
  }
}

const debounce = (callback, wait) => {
  let timeoutId = null;
  return (...args) => {
    window.clearTimeout(timeoutId);
    timeoutId = window.setTimeout(() => {
      callback.apply(null, args);
    }, wait);
  };
}

const markValid = (input) => {
  input.classList.remove('is-invalid')
  input.classList.add('is-valid')
}

const markInvalid = (input) => {
  input.classList.remove('is-valid')
  input.classList.add('is-invalid')
}

function networkFromAddress(address) {
  try {
    if (address[0] == "b") {
      new bitcoin.address.fromBech32(address)
      return 'mainnet'
    } else if (address[0] == "t") {
      new bitcoin.address.fromBech32(address)
      return 'testnet'
    }

    const btcAddress = new bitcoin.address.fromBase58Check(address)
    if (btcAddress.version == 0 || btcAddress.version == 5) {
      return 'mainnet'
    } else if (btcAddress.version == 111 || btcAddress.version == 196) {
      return 'testnet'
    } else {
      throw "unknow address format"
    }
  } catch(e) {
    if (e.message.match(/Invalid checksum/)) {
      return null
    }
    throw e
  }
}

function parseAddress(address) {
  const network = networkFromAddress(address)
  if (!network) {
    return {address, network: null}
  }
  return {address, network}
}

export {markInvalid, markValid, networkFromAddress, parseAddress, debounce, throttle}
